package it.dtk

import akka.testkit.{ TestKit, ImplicitSender }
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpecLike }
import akka.actor.{ ActorSystem, Props }
import scala.concurrent.duration._
import it.dtk.FrontierDepth._

object FrontierSpec {
  val urls1 = Set("a", "b", "c", "d")
  val urls2 = Set("a", "b", "f")
  val url1 = "a"
  val url2 = "b"

  val param = 10
}

class FrontierSpec extends TestKit(ActorSystem("FrontierSpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {
  import FrontierSpec._

  override def afterAll() {
    TestKit.shutdownActorSystem(system)
  }
  val frontierActor = system.actorOf(Props(new FrontierDepth(param)))

  "A Frontier Actor" must {
    "store a set of urls" in {
      frontierActor ! Insert(url1, Set(url1))
      frontierActor ! GetUrlToVisit
      val result = expectMsgClass(10.seconds, classOf[UrlToVisit])
      result.urls.size should be equals (urls1.size)
    }

    "send a required url" in {
      frontierActor ! Head
      val result = expectMsgClass(10.seconds, classOf[TopUrl])

      result.url.get should not be null
      //result should not be null
      println("URL in testa: " + result.url.get)
    }

  }

}