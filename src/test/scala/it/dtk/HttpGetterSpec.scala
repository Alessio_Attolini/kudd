package it.dtk

import scala.concurrent.duration._
import akka.actor.Props
import it.dtk.util.MySpec
import org.joda.time.DateTime

/**
 * @author Andrea Scarpino <andrea@datatoknowledge.it>
 */
class HttpGetterSpec extends MySpec("HttpGetterSpec") {

  import HttpGetter._

  "An HttpGetter actor" must {

    val getter = system.actorOf(Props[HttpGetter])

//    "solves illegal character" in {
//
//      getter ! Get("http://www.repubblica.it/####")
//      val res = expectMsgClass(100.seconds, classOf[Result])
//      res.html.getClass should be(classOf[String])
//      //res.headerDate.getClass should be(classOf[Date])
//    }

    "check malformed escape pair" in {

      getter ! Get("http://www.repubblica.it/argomenti/tetto_del_3%/")
      val res = expectMsgClass(200.seconds, classOf[Fail])
//      res.html.getClass should be(classOf[String])
      //res.headerDate.getClass should be(classOf[Date])
    }

  }

}
