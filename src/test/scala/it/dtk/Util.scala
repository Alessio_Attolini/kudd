package it.dtk

import akka.testkit.TestKit
import akka.actor.ActorSystem
import org.scalatest.WordSpecLike
import org.scalatest.BeforeAndAfterAll
import akka.testkit.ImplicitSender
import org.scalatest.Matchers
import akka.actor.Props
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Terminated

class MyTestStruct(name: String) extends TestKit(ActorSystem(name))
  with WordSpecLike with BeforeAndAfterAll with ImplicitSender with Matchers

class StepParent(child: Props, fwd: ActorRef) extends Actor {
  context.actorOf(child, "child")
  def receive = {
    case Terminated(ref) => println(ref)
    //fwd.tell(t, sender)
    case msg =>
      fwd.tell(msg, sender)
  }
}