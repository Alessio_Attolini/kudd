package it.dtk

import akka.testkit.{ TestKit, ImplicitSender }
import akka.actor.{ ActorSystem, Props }
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpecLike }
import scala.concurrent.duration._
import java.util.Collection

object HtmlParserSpec {

  val url = "http://www.barirepubblica.it"

  /* File valido */
  val rightHtml = """
		<!doctype html>
		<head>	
		<meta charset="utf-8">
		<title>Giovani, anziani, asili nido e soldi per il Sud ecco il
			progetto del governo per l'equit&agrave;&nbsp; - Economia e Finanza con
			Bloomberg - Repubblica.it</title>
		
		<meta name="keywords" content="Economia, " />
		<meta name="description"
			content="Fondi Ue anche per l'innovazione delle imprese. Aiuti straordinari per il Mezzogiorno. Monti: &quot;Abbiamo messo la giustizia sociale al centro della nostra azione&quot;. Litigano Pd e Pdl sull'efficacia del progetto. Ma &egrave; buona l'accoglienza dei governatori meridionali. Camusso: &quot;E' " />
		<meta name="titolo"
			content="Giovani, anziani, asili nido e soldi per il Sud ecco il progetto del governo per l'equit&agrave;&nbsp;" />
		<meta name="timeModifiedOra" content="05:40:00" />
		<meta name="subSez" content="" />
		<meta property="og:type" content="article" />
		</head>
		<body class="detail">
		 <ul class="menu_list_ul">
            <li><a href="Default.aspx" id="lnkHome" class="blue_text_12">Home</a></li>
            <li><span id="hp" style="behavior:url(#default#homepage);"></span></li>
        </ul>
		</body>
		</html>
    """

  val rightTitle = "Giovani, anziani, asili nido e soldi per il Sud ecco il progetto del governo per l'equità"
  //val lines = scala.io.Source.fromFile("TestArticle.html", "utf-8").getLines().mkString

  /* Link non validi */
  val invalidLink = """
    <!doctype html>
		<head>	
		<meta charset="utf-8">
     <ul class="menu_list_ul">
            <li><a hre="Default.aspx" id="lnkHome" class="blue_text_12">Home</a></li>
            <li><span id="hp" style="behavior:url(#default#homepage);"></span></li>
        </ul>
    """

  val relativeUrls = """ 
       <li><a href="/sport/moto-gp/2">2</a></li>
	            <li><a href="/sport/moto-gp/3">3</a></li>
	            <li><a href="/sport/moto-gp/4">4</a></li>
	            <li><a href="/sport/moto-gp/5">5</a></li>
	            <li><a href="/sport/moto-gp/6">6</a></li>
	            <li><a href="/sport/moto-gp/7">7</a></li>
	            <li><a href="/sport/moto-gp/8">8</a></li>
	            <li><a href="/sport/moto-gp/9">9</a></li>
      """

  val absoluteUrls = """
    <a href="http://www.repubblica.it/ambiente/">Ambiente</a>
                            <a href="http://www.repubblica.it/arte/">Arte</a>
                            <a href="http://design.repubblica.it/">Casa</a>
                            <a href="http://www.repubblica.it/cronaca/">Cronaca</a>
                            <a href="http://www.repubblica.it/cultura/">Cultura</a>
                            <a href="http://www.repubblica.it/economia/">Economia</a>
                            <a href="http://www.repubblica.it/esteri/">Esteri</a>
                            <a href="http://www.repubblica.it/gallerie/">Foto</a>
                            <a href="http://www.repubblica.it/le-interviste/">Interviste</a>
                            <a href="http://www.repubblica.it/solidarieta/">Mondo Solidale</a>
                            <a href="http://www.repubblica.it/motori/">Motori</a>
        """

  /* Meta tag non validi */
  val invalidMeta = """
		<!doctype html>
		<head>	
		<meta charset="utf-8">
		<title>Giovani, anziani, asili nido e soldi per il Sud ecco il
			progetto del governo per l'equit&agrave;&nbsp; - Economia e Finanza con
			Bloomberg - Repubblica.it</title>
		
		<met name="keywords" content="Economia, " />
		
		</head>
		</html>
    """

  /* Title assente */
  val invalidTitle = """
		<!doctype html>
		<head>		
		</head>
		</html>
    """
}

class HtmlParserSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  import HtmlParser._
  import HtmlParserSpec._

  def this() = this(ActorSystem("HtmlParserSpec"))

  override def afterAll() {
    TestKit.shutdownActorSystem(system)
  }

  "A HtmlParser Actor" must {

    //        "return data parser" in {
    //          system.actorOf(Props(new StepParent(Props(new HtmlParser(url, rightHtml)), testActor)), "rightParsing")
    //          val res = expectMsgClass(1000.seconds, classOf[Result])
    //          res.title should be equals (rightTitle)
    //          res.metaTags should not be null
    //          res.outlinks.size should be equals (1)
    //          val elems = res.outlinks.keySet
    //        }
    //
    //    "recognize absent or invalid title" in {
    //      system.actorOf(Props(new StepParent(Props(new HtmlParser(url, invalidTitle)), testActor)), "invalidTitle")
    //      expectMsg(FailTitle(url))
    //    }
    //
    //    "recognize absent or invalid links" in {
    //      system.actorOf(Props(new StepParent(Props(new HtmlParser(url, invalidLink)), testActor)), "invalidLink")
    //      expectMsg(FailLinks(url))
    //    }
//
//    "recognize absolute urls" in {
//      system.actorOf(Props(new StepParent(Props(new HtmlParser(url, absoluteUrls)), testActor)), "absoluteUrls")
//      val res = expectMsgClass(1000.seconds, classOf[Result])
//    }

    "recognize relative urls" in {
      val htmlParserActor = system.actorOf(Props[HtmlParser])
      htmlParserActor ! Parse(url, relativeUrls)
      val res = expectMsgClass(1000.seconds, classOf[Result])
    }

    //    "recognize absent or invalid meta tags" in {
    //      system.actorOf(Props(new StepParent(Props(new HtmlParser(url, invalidMeta)), testActor)), "invalidMeta")
    //      expectMsg(FailMeta(url))
    //    }
  }

}