package it.dtk

import akka.testkit.TestKit
import akka.testkit.ImplicitSender
import org.scalatest.BeforeAndAfterAll
import akka.actor.ActorSystem
import org.scalatest.Matchers
import org.scalatest.WordSpecLike
import akka.actor.Props
import it.dtk.Cache._

object CacheSpec {
  val url = "www.google.it"
  val html = "<html></html>"
  val url2 = "www.diretta.it"
  val html2 = "<head></head>"
}

class CacheSpec extends TestKit(ActorSystem("CacheSpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  import CacheSpec._

  "A Cache Actor" must {
//    "put succesfully an html page" in {
//      val cacheActor = system.actorOf(Props[Cache])
//      //send save url html message
//      cacheActor ! Put(url, html)
//      //expext succes with url as parameter
//      expectMsg(PutResult(url))
//    }

    "get an html page already stored" in {
      val cacheActor = system.actorOf(Props[Cache])
      cacheActor ! Put(url2, html2)
      expectMsg(PutResult(url2))
      cacheActor ! Get(url2)
      expectMsg(Found(url2))//, Some(html2)))
    }
  }
}