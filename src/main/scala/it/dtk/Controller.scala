package it.dtk

import akka.actor.Actor
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy
import akka.actor.Props
import akka.actor.ActorLogging
import akka.actor.Terminated
import akka.actor.ReceiveTimeout
import java.util.Date
import akka.actor.ActorRef
import akka.routing.RoundRobinRouter
import akka.routing.RoundRobinGroup
import akka.routing.RoundRobinPool
import akka.routing.RoundRobinPool

object Controller {
    case class Start(urlStart: String)
    case class Success(startUrl: String, urlCrawled: Set[String])
    case class Fail(startUrl: String)
}

/**
 * @author Fabio
 *
 */
class Controller(typeSearc: String, limit: Int, overDomain: Boolean) extends Actor with ActorLogging {

    import Controller._

    //TODO verificare strategia
    override val supervisorStrategy = OneForOneStrategy(maxNrOfRetries = 5) {
        case _: Exception =>
            SupervisorStrategy.Restart
    }

    private val parallelFactor = 10
    private var activeJobs = 0
    private var startUrl = ""

    val httpGetterRouter = context.actorOf(RoundRobinPool(parallelFactor).props(Props[HttpGetter]))
    val htmlParserRouter = context.actorOf(RoundRobinPool(parallelFactor).props(Props[HtmlParser]))
    val cacheActor = context.actorOf(Props[Cache])
    var frontierActor = context.actorOf(Props(new FrontierDepth(limit)))

    //  if (typeSearc.equals("depth")) {
    //    frontierActor = context.actorOf(Props(new FrontierDepth(limit)))
    //  } else {
    //    frontierActor = context.actorOf(Props(new Frontier(limit)))
    //  }

    def receive = waiting

    def waiting: Receive = {
        case Start(urlStart: String) =>
            startUrl = urlStart
            log.info("[WAITING-Act.Job:{}-Chil.s:{}] Starting crawler with url {}", activeJobs, context.children.size, urlStart)
            frontierActor ! Frontier.Insert(urlStart, Set(urlStart))
            context.become(running)
            frontierActor ! Frontier.Head
        case msg =>
            log.info("WAITING: unhandled message: {}", msg)
    }

    def running: Receive = {
        case Frontier.TopUrl(Some(url)) =>
            //      cacheActor ! Cache.Get(url)
            activeJobs = activeJobs + 1
            log.info("[RUNNING-Act.Job:{}-Chil.s:{}] Processing url {}", activeJobs, context.children.toList.length, url)
            httpGetterRouter ! HttpGetter.Get(url)
        case Cache.Found(url) => //, result) =>
            log.info("[RUNNING-Act.Job:{}-Chil.s:{}] {} was already stored in Cache", activeJobs, context.children.toList.length, url)
            frontierActor ! Frontier.Head
        case Cache.NotFound(url) =>
            activeJobs = activeJobs + 1
            log.info("[RUNNING-Act.Job:{}-Chil.s:{}] Processing url {}", activeJobs, context.children.toList.length, url)
            httpGetterRouter ! HttpGetter.Get(url)
        case Frontier.TopUrl(None) =>
            log.info("[RUNNING-Act.Job:{}-Chil.s:{}] Empty Frontier", activeJobs, context.children.toList.length)
            if (activeJobs == 0) {
                context.become(stop)
                frontierActor ! Frontier.GetUrlVisited
            }
        case HttpGetter.Result(url, html, date) =>
            log.info("[RUNNING-Act.Job:{}-Chil.s:{}] HTTPGetter processed url {}", activeJobs, context.children.toList.length, url)
            //      cacheActor ! Cache.Put(url, html)
            htmlParserRouter ! HtmlParser.Parse(url, html)
        case Cache.PutResult(url) =>
            log.info("[RUNNING-Act.Job:{}-Chil.s:{}] {} stored in Cache", activeJobs, context.children.toList.length, url)
        case HttpGetter.Fail(url, statusCode) =>
            decrementNumJobs
            log.info("[RUNNING-Act.Job:{}-Chil.s:{}] HTTPGetter failed to process url {}", activeJobs, context.children.toList.length, url)
            generateNewJobs
        case HtmlParser.Result(url, outlinks, title, metaTags) =>
            //inviare mess al DBActor  
            decrementNumJobs
            log.info("[RUNNING-Act.Job:{}-Chil.s:{}-{}] HTMLParser obtained {} outlinks", activeJobs, context.children.toList.length, context.sender, outlinks.size)
            frontierActor ! Frontier.Insert(url, outlinks.keySet.toSet)
            generateNewJobs
        case HtmlParser.Fail(url) =>
            decrementNumJobs
            log.info("[RUNNING-Act.Job:{}-Chil.s:{}] HTMLParser failed to process url {}", activeJobs, context.children.toList.length, url)
            generateNewJobs
        case Terminated(ref) =>
            log.info("Got a death letter from {}", ref.toString)
            context.become(stop)
            Frontier.GetUrlVisited
        case msg =>
            log.info("[RUNNING] hunandled message: {}", sender.toString)
    }

    def generateNewJobs(): Unit = {
        for (newJob <- activeJobs until parallelFactor)
            frontierActor ! Frontier.Head
    }

    def decrementNumJobs(): Unit = {
        activeJobs = activeJobs - 1
    }

    def stop: Receive = {
        case Frontier.UrlVisited(urls) =>
            context.parent ! Success(startUrl, urls)
        //      println("Controller STOP")
        //      context.children foreach (context.stop(_))
        //      context.stop(sender) //stops the controller
    }
}