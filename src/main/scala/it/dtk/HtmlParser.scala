package it.dtk

import akka.actor.Actor
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import scala.collection.JavaConversions._
import org.joda.convert.ToString
import com.sun.org.apache.xml.internal.serializer.ToStream
//import edu.uci.ics.crawler4j.url._
//import edu.uci.ics.crawler4j.crawler._
import java.net.URL
import scala.util.Try

object HtmlParser {
	case class Result(url: String, outlinks: Map[String, List[String]], title: String, metaTags: Map[String, String])
	case class Parse(url: String, html: String)
	case class Fail(url: String)

	val outsideDomains = false // Set TRUE for add urls out of domain

}

/**
 * @author Alessio & Fabio
 *  it extracts from the HTML:
 *  <ul>
 *  	<li>outlinks Map[url, List[anchor-texts]</li>
 *  <li>title</li>
 *  <li>list of meta tags</li>
 *  </ul>
 *
 */
class HtmlParser extends Actor {

	import HtmlParser._

	def receive = {
		case Parse(url, html) =>
			val result = Try { // Using Jsoup for parsing the HTML source
				val doc = Jsoup.parse(html, url)
				val title = doc.title()
				val links = doc.select("a[href]")
				val meta = doc.select("meta[name]")

				val outLinks = getOutLinks(url, links)
				val metaTags = getMetaTags(meta)
				(outLinks, title, metaTags)
			}

			if (result.isSuccess)
				sender ! Result(url, result.get._1, result.get._2, result.get._3)
			else
				sender ! Fail(url)
	}

	/*
	 * It allows the recovery of the outlinks from the page linked by
	 * the URL in input.
	 *
	 * @param url		the URL of the page in exam.
	 * @param links	the list of outlinks
	 * @return the maps url -> list[anchor text]
	 */
	private def getOutLinks(url: String, links: Elements): Map[String, List[String]] = {
		val baseUrl = new URL(url)

		/* It creates a buffer of (link, absolute url) pairs. 
		 * Elements.attr() extract an attribute.
		 * Elements.text() give the text of the element.
		 */
		val bufferTextHref = links.map { e =>
			e.attr("abs:href") -> e.text()
		}

		/* It filters the empty links and those links that are equals to base URL.*/
		val filteredTextHref = bufferTextHref.filter(pair => pair._1 != "").filter { pair =>
			val url = new URL(pair._1)
			baseUrl.getHost() == url.getHost()
		}
		val grouByUrl = filteredTextHref.groupBy(p => p._1)
		val mapHrefTexts = grouByUrl.map(keyValue => keyValue._1 -> keyValue._2.map(pair => pair._2).toList)

		mapHrefTexts
	}

	private def getMetaTags(meta: Elements): Map[String, String] = {
		var metaMap = Map.empty[String, String]
		if (meta != null)
			meta.foreach { n =>
				//controllare se ha senso togliere sempre il comma
				val content = n.attr("content").mkString.replaceAll(",", "")
				val name = n.attr("name").toString()

				metaMap += content -> name
			}
		metaMap
	}
}