package it.dtk

import akka.actor.{ Actor, Props, ActorRef }
import akka.actor.ActorSystem
import java.io.File
import java.io.FileWriter

object Receptionist {
  private case class Job(client: ActorRef, url: String, limit: Int)
  //  case class Crawl(url: String, limit: Int)
  case class Get(url: String, limit: Int)
  case class Result(url: String, links: Set[String])
  case class Failed(url: String)
}

/**
 * @author Alessio
 * it should run k controller in parallel via jobs
 *
 */
class Receptionist extends Actor {
  import Receptionist._

  var requestNumber = 0

  def receive = waiting

  val waiting: Receive = {
    case Get(url, limit) => context.become(runNext(Vector(Job(sender, url, limit))))
  }

  def running(queue: Vector[Job]): Receive = {
    case Controller.Success(startUrl: String, urlCrawled: Set[String]) =>
      val job = queue.head
      job.client ! Result(job.url, urlCrawled)
      //      context.stop(sender) //stops the controller
      context.become(runNext(queue.tail))
    case Controller.Fail(url) =>
      val job = queue.head
      job.client ! Failed(url)

    case Get(url, limit) =>
      context.become(enqueueJob(queue, Job(sender, url, limit)))
  }

  def runNext(queue: Vector[Job]): Receive = {
    requestNumber += 1
    if (queue.isEmpty)
      waiting
    else {
      val controller = context.actorOf(Props(new Controller("depth", queue.head.limit, false)), s"c$requestNumber")
      val head = queue.head
      controller ! Controller.Start(head.url)
      running(queue)
    }
  }

  def enqueueJob(queue: Vector[Job], job: Job): Receive = {
    if (queue.size > 3) {
      sender ! Failed(job.url)
      running(queue)
    } else
      running(queue :+ job)
  }
}

/**
 * ****************************** MAIN ********************************
 */
object Main extends App {

  val system = ActorSystem("KUDD")
  val receptionist = system.actorOf(Props[ReceptionistMain], "ReceptionistMain")
  //  val url1 = "http://www.brindisireport.it/"
  //  val url2 = "http://www.repubblica.it/argomenti/tetto_del_3%/"
  //  val url2 = "http://www.baritoday.it/"
  //  val url3 = "http://corrieredelmezzogiorno.corriere.it/"
  //  val url4 = "http://www.giornaledipuglia.com/"
  //  val url5 = "http://www.diretta.it/"
  val url6 = "http://www.puglia24news.it/"
  val limit = 1
}

class ReceptionistMain extends Actor {

  import Receptionist._
  import Main._

  val receptionist = context.actorOf(Props[Receptionist], "Receptionist")

  var startTime = 0.0;
  var endTime = 0.0;
  var duration = 0.0;

  startTime = System.currentTimeMillis();
  //	receptionist ! Get(url1, limit)
  //	receptionist ! Get(url2, limit)
  //	receptionist ! Get(url3, limit)
  //	receptionist ! Get(url4, limit)
  //	receptionist ! Get(url5, limit)
  receptionist ! Get(url6, limit)

  def receive = {
    case Result(url, numUrls) =>
      println("Ho analizzato " + numUrls.size + " elementi partendo da " + url)
      endTime = System.currentTimeMillis()
      duration = (endTime - startTime) / 1000
      println("\nTempo impiegato: " + duration + "s")
      //      context.stop(self)
      val file = new File("./results/" + url.substring(7, url.length() - 1) + ".txt")
      val writer = new FileWriter(file)
      writer.write(url + " depth: " + limit + " tempo: " + duration + " link: " + numUrls.size)
      writer.flush
      writer.close

    case Controller.Fail(url) =>
      println("Crawling fallito per l'url: " + url)
      endTime = System.currentTimeMillis()
      duration = (endTime - startTime) / 1000
      println("\nTempo impiegato: " + duration + "s")
      context.stop(self)
  }

  override def postStop(): Unit = {
    AsyncWebClient.shutdown()
  }
}