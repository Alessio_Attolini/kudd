package it.dtk

import akka.actor.Actor
import akka.actor.ActorLogging

/**
 * @author Alessio
 * it should stores URLs to visit and visited URLs.
 */
object Frontier {
    case object Head
    case class TopUrl(url: Option[String])
    case class Insert(startUrl: String, urls: Set[String])
    case object GetUrlVisited
    case object GetUrlToVisit
    case class UrlVisited(urls: Set[String])
    case class UrlToVisit(urls: Set[String])
}

/**
 * While urlVisited<maxNumber, frontier sends to Controller an URL.
 */
class Frontier(var maxNumberUrls: Int) extends Actor with ActorLogging {

    import Frontier._

    var urlVisited = Set.empty[String]
    var urlToVisit = Set.empty[String]
    var maxNumber = maxNumberUrls

    def receive = {

        /* Estrae l'url di testa da visitare e lo inserisce nei visitati.
    	 * Infine invia l'url all'attore chiamante. 
     	*/
        case Head => {
            val message = {
                if (urlVisited.size <= maxNumber) {
                    val current = urlToVisit.head
                    urlToVisit = urlToVisit.tail
                    urlVisited += current
                    TopUrl(Some(current))
                } else if (urlToVisit.isEmpty) {
                    TopUrl(None)
                } else {
                    TopUrl(None)
                }
            }
            sender ! message
        }

        case Insert(startUrl, urls) => {
            val toVisit = urls &~ urlVisited // &~ stands for difference
            urlToVisit ++= toVisit
        }

        case GetUrlVisited => {
            sender ! UrlVisited(urlVisited)
        }

        case GetUrlToVisit => {
            sender ! UrlToVisit(urlToVisit)
        }
    }
}