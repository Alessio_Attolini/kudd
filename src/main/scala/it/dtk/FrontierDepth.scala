package it.dtk

import akka.actor.Actor
import akka.actor.ActorLogging

/**
 * @author Alessio
 * it should stores URLs to visit and visited URLs.
 */
object FrontierDepth {
  case object Head
  case class TopUrl(url: Option[String])
  case class Insert(startUrl: String, urls: Set[String])
  case object GetUrlVisited
  case object GetUrlToVisit
  case class UrlVisited(urls: Set[String])
  case class UrlToVisit(urls: Set[String])
}

class FrontierDepth(var depth: Int) extends Actor with ActorLogging {

  import FrontierDepth._
  //è un Map in quanto bisogna associare ad ogni url la sua profondità
  var urlVisited = Map.empty[String, Int]
  var urlToVisit = Map.empty[String, Int]
  val maxDepth = depth

  def receive = {
    //prende elimina la testa della lista da visitare e la inserisce nei visitati, infine invia l'el al parent
    case Head => {
      val message = if (urlToVisit.keySet.isEmpty) {
        TopUrl(None)
      } else {
        //risponde con un messaggio(ResEmpty) solo se la coda degli url da visitare non è vuota 
        val current = urlToVisit.head
        urlToVisit = urlToVisit.tail
        urlVisited += current
        TopUrl(Some(current._1))
      }
      sender ! message
    }

    case Insert(startUrl, urls) => {
      //devo verificare che ogni url non è presente tra le keys di UrlVisited
      if (urlToVisit.isEmpty & urlVisited.isEmpty) {
       urlToVisit += (startUrl -> 0)
      } else if(urlVisited.get(startUrl).get<maxDepth){
        //compute the difference between the urls and the visited urls: &~ stands for difference
        val toVisit = urls &~ urlVisited.keySet
        var depthUrls = urlVisited.get(startUrl).get + 1
        toVisit.map(s => urlToVisit += (s -> depthUrls))
        var set =Set.empty[Int]
        println("*******************************"+urlToVisit.keySet.map(s=>set + urlToVisit.get(s).get))
        println()
        //keys map{s=> (s->1)} toMap
        //urlToVisit ++= toVisit
      }
    }

    case GetUrlVisited => {
      sender ! UrlVisited(urlVisited.keySet)
    }

    case GetUrlToVisit => {
      sender ! UrlToVisit(urlToVisit.keySet)
    }
  }
}