package it.dtk

import akka.actor.Actor
import akka.actor.ActorLogging
import it.dtk.util.Hasher
import scala.io.Source
import java.io.{ FileReader, FileNotFoundException, IOException }
import java.io.FileWriter
import java.nio.file.Files
import java.nio.file.Paths
import scala.util.Try
import java.nio.file.Path
import java.io.File

object Cache {
  case class Get(url: String)
  case class Found(url: String) //, html: Option[String])
  case class NotFound(url: String)
  case class Put(url: String, html: String)
  case class PutResult(url: String)

  private val basePath = "./cache/"

}

/**
 * @author Alessio & Fabio
 * NOSQL DB Key Value, column oriented suggested Cassandra
 */
class Cache extends Actor with ActorLogging {
  import Cache._

  Files.createDirectories(Paths.get(basePath))

  def receive = {
    case Get(url) =>
      val hash = Hasher.convert(url).replace("/", "")
      if (exist(hash))
        sender ! Found(url)
      else
        sender ! NotFound(url)

    case Put(url, html) =>
      val hash = Hasher.convert(url).replace("/", "")
      val file = new File(basePath + hash + ".txt")
      val writer = new FileWriter(file)
      writer.write(html)
      writer.flush
      writer.close

      sender ! PutResult(url)

  }

  private def exist(hash: String): Boolean = Files.exists(Paths.get(basePath + hash + ".txt"))

  private def get(hash: String): Option[String] = {
    try {
      val html = Source.fromFile(basePath + hash + ".txt").getLines.mkString
      Some(html)
    } catch {
      case ex: Exception => None
    }

  }
}