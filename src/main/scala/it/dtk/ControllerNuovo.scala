//package it.dtk
//
//import akka.actor.Actor
//import akka.actor.OneForOneStrategy
//import akka.actor.SupervisorStrategy
//import akka.actor.Props
//import akka.actor.ActorLogging
//import akka.actor.Terminated
//import akka.actor.ReceiveTimeout
//import java.util.Date
//import akka.actor.ActorRef
//import akka.actor.Terminated
//import akka.routing.RoundRobinRouter
//
//object ControllerNuovo {
//  case class Start(startUrl: String)
//  case class Success(startUrl: String, sizeUrLCrawled: Int)
//  case class Fail(startUrl: String)
//  //  case object Finished
//
//}
//
///**
// * @author Fabio
// *
// */
//class ControllerNuovo extends Actor with ActorLogging {
//
//  import ControllerNuovo._
//  import Frontier._
//  import HtmlParser._
//  import Cache._
//
//  override val supervisorStrategy = OneForOneStrategy(maxNrOfRetries = 5) {
//    case _: Exception => SupervisorStrategy.Restart
//  }
//
//  // Max concurrent getter actors
//  val parallelFactor = 10
//  //seleziona i fratelli del controller avente nome "cache"
//  //def cacheProps = cache
//
//  val httpGetterRouter = context.actorOf(Props[HttpGetter].withRouter(RoundRobinRouter(nrOfInstances = parallelFactor)))
//
//  val htmlParserRouter = context.actorOf(Props[HtmlParser].withRouter(RoundRobinRouter(nrOfInstances = parallelFactor)))
//
//  val frontierActor = context.actorOf(Props[Frontier])
//
//  val cacheActor = context.actorOf(Props[Cache])
//
//  var startUrl = ""
//
//  private var activeJobs = 0
//  private var idHTMLpar = 1
//  //private var currentStop = -1
//
//  def receive = waiting
//
//  def waiting: Receive = {
//    case Start(startUrl: String) =>
//      log.info("[WAITING-Act.Job:{}-Chil.s:{}] Starting crawler with url {}", activeJobs, context.children.size, startUrl)
//      cacheActor ! Get(startUrl)
//
//    case Cache.GetResult(url, optHtml) =>
//
//      optHtml match {
//        case Some(html) =>
//          log.info("[WAITING-Act.Job:{}-Chil.s:{}] First url already stored in Cache", activeJobs, context.children.toList.length)
//        if(activeJobs==0)
//          context.become(stop)
//        case None =>
//          //          frontierActor ! Insert(Set(url))
//          //          frontierActor ! Head
//          //          context.become(running)
//          startUrl = url
//          httpGetterRouter ! HttpGetter.Get(url)
//          context.become(running)
//      }
//
//    case msg =>
//      log.info("WAITING: c'è qualcosa che non va: {}", msg)
//  }
//
//  def running: Receive = {
//
//    case HttpGetter.Result(url, html, date) =>
//      log.info("[RUNNING-Act.Job:{}-Chil.s:{}] HTTPGetter processed url {}", activeJobs, context.children.toList.length, url)
//      //idHTMLpar = idHTMLpar + 1
//      htmlParserRouter ! Parse(url, html)
//      cacheActor ! Put(url, html)
//
//    case HttpGetter.Fail(url, statusCode) =>
//      activeJobs = activeJobs - 1
//      log.info("[RUNNING-Act.Job:{}-Chil.s:{}] HTTPGetter failed to process url {}", activeJobs, context.children.toList.length, url)
//      frontierActor ! Head
//
//    case Frontier.TopUrl(Some(url)) =>
//      log.info("[RUNNING-Act.Job:{}-Chil.s:{}] Processing url {}", activeJobs, context.children.toList.length, url)
//      //      httpGetterRouter ! HttpGetter.Get(url)
//      //      activeJobs = activeJobs + 1
//      cacheActor ! Get(url)
//
//    case HtmlParser.Result(url, outlinks, title, metaTags) =>
//      //inviare mess al DBActor  
//      activeJobs = activeJobs - 1
//      log.info("[RUNNING-Act.Job:{}-Chil.s:{}-{}] HTMLParser obtained {} outlinks", activeJobs, context.children.toList.length, context.sender, outlinks.size)
//      frontierActor ! Insert(url, outlinks.keySet.toSet)
//      //var newJob = 0
//      for (newJob <- activeJobs until parallelFactor)
//        frontierActor ! Head
//
//    case Frontier.TopUrl(None) =>
//      if (activeJobs == 0) {
//        frontierActor ! GetUrlVisited
//        context.become(stop)
//      } else {
//        log.info("[RUNNING-Act.Job:{}-Chil.s:{}] Empty Frontier", activeJobs, context.children.toList.length, context.sender)
//      }
//
//    case Cache.GetResult(url, optHtml) =>
//      if (optHtml.isEmpty) {
//        //frontierActor ! Insert(Set(url))
//        //context.become(running)
//        httpGetterRouter ! HttpGetter.Get(url)
//      } else {
//        log.info("[RUNNING-Act.Job:{}-Chil.s:{}] Url {} already stored in Cache", activeJobs, context.children.toList.length, context.sender)
//      }
//
//    case Cache.PutResult(url) =>
//      log.info("[RUNNING-Act.Job:{}-Chil.s:{}] {} stored in Cache!", activeJobs, context.children.toList.length, url)
//    // frontierProps ! Insert(Set(url))
//
//    //        case Frontier.AllProcessed =>
//    //          log.info("[RUNNING-Act.Job:{}-Chil.s:{}] all urls processed", activeJobs, context.children.toList.length)
//    //          if (activeJobs == 0) {
//    //            context.become(stop)
//    //            self ! Stop
//    //          }
//
//    case Terminated(ref) =>
//      log.info("Got a death letter from {}", ref.toString)
//
//    case ReceiveTimeout =>
//      log.info("[RUNNING-Act.Job:{}-Chil.s:{}] Failure during the crawling!", activeJobs, context.children.toList.length)
//      context.children foreach context.stop
//      context.parent ! ControllerNuovo.Fail
//
//    case msg =>
//      log.info("RUNNING: c'è qualcosa che nn va: {}", msg)
//  }
//
//  def stop: Receive = {
//
//    case UrlVisited(urls) =>
//      sender ! Success(startUrl, urls.size)
//      println("Controller STOP")
//      context.children foreach context.stop
//      context.stop(self)
//  }
//}
//
