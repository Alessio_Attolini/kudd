package it.dtk

import akka.actor.Actor

/**
 * @author 
 * to investigate Nutch results
 */
object DBActor{
  case class Page()
  case class Link()
  case class CrawledElement()
}

/**
 * @author Alessio & Fabio
 * it should connect to Cassandra
 */
trait DBActor extends Actor{

  
}