//import akka.actor.{ Actor, Props, ActorSystem }
//import it.dtk.ControllerNuovo
//import it.dtk.Controller
//
//object Main extends App {
//  
//  import it.dtk.Controller._
//
//  val system = ActorSystem("KUDD")
//  val mainActor = system.actorOf(Props[MainActor], "MainActor")
//  val url = "http://www.tranilive.it/"
//  mainActor ! MainActor.Crawl(url)
//
//  //  array.foreach { n =>
//  //    mainActor ! MainActor.Crawl(n)
//  //  }
//}
//
//object MainActor {
//  case class Crawl(url: String)
//}
//
//class MainActor extends Actor {
//
//  val controller = context.actorOf(Props[ControllerNuovo], "ControllerNuovo")
//
//  def receive = {
//
//    case MainActor.Crawl(url) =>
//      controller ! ControllerNuovo.Start(url)
//
//    case ControllerNuovo.Success(url, numUrls) =>
//      println("Ho analizzato " + numUrls + " elementi partendo da " + url)
//    case ControllerNuovo.Fail(url) =>
//      println("Crawling fallito per l'url: " + url)
//  }
//
//}